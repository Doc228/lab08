#include <stdio.h> //Лаба №5	23. (**) Визначити, чи є задане ціле число простим.
#include "main.h"
int main()
{
	first_type();// Вариант через цыкл for
	second_type();// Вариант через цыкл whilе
	third_type();// Вариант через цыкл do
	return 0;
}
void first_type()
{
	int number = 8, i, b = 0; // number - заданное число
	for (i = 2; i != number; i++) {// берем отсчет с двоечки, так как на единицу делятся все цыфры.
		if (number % i == 0) {
			b = b + 1;
		}
	} //Число простое, если b = 0
}
void second_type()
{
	int number = 8, i = 2, b = 0; // number - заданное число
	while (i != number) {
		if (number % i == 0) {
			b = b + 1;
		}
		i++;
	} //Число простое, если b = 0
}
void third_type()
{
	int number = 8, i = 2, b = 0; // number - заданное число
	do {
		if (number % i == 0) {
			b = b + 1;
		}
		i++;
	} while (i != number);
} //Число простое, если b = 0
